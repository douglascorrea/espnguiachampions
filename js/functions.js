$(document).imagesLoaded( function() {

    var playing = true;

//    $(".btn-som").on("click", function () {
//
//        $(this).toggleClass("parar");
//        if (playing == false) {
//            document.getElementById('player').play();
//            playing = true;
//        } else {
//            document.getElementById('player').pause();
//            playing = false;
//        }
//    });

    var _comscore = _comscore || [];
    _comscore.push({ c1: "2", c2: "15382524" });
    (function () {
        var s = document.createElement("script"), el = document.getElementsByTagName("script")[0];
        s.async = true;
        s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
        el.parentNode.insertBefore(s, el);
    })();

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-39636870-1']);
    _gaq.push(['_setDomainName', 'uol.com.br']);
    _gaq.push(['_addIgnoredRef', 'espn.uol.com.br']);
    _gaq.push(['_setCustomVar', 4, 'Site layout', 'WWW (padrão)', 2]);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);
    setTimeout("_gaq.push(['_trackEvent', '15_segundos', 'read'])", 15000);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

    (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-MQ6GW6');

    window.fbAsyncInit = function () {
        FB.init({
//            appId      : '1456673401251932',
//            appId      : '682341765130248',
            appId: '293194120847793',
            xfbml: true,
            version: 'v2.0'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $('.shareFbTopo').on('click', function (e) {
        e.preventDefault();
        var section = $(this).data('section');
        FB.ui({
            method: 'feed',
            link: 'http://espn.uol.com.br/infografico/guia-uefa-champions-league-2014/',
            picture: 'http://static.espn.com.br.s3-website-us-east-1.amazonaws.com/infografico/guia-uefa-champions-league-2014/images/facebook_share.png',
            name: 'Guia ESPN UEFA Champions League 2014',
            caption: 'Confira todas as novidades, equipes, craques e as novas promessas da UEFA Champions League 2014!',
            description: 'Compartilhe o guia mais completo da competição!'
        });
        _gaq.push(['_trackSocial', '', 'Compartilhar', ''], ['_trackEvent', 'SHARE-NFL-topo', '', 'topo']);
    });

	
	/**
	Script tests browser features and tells if the Browser is IE10 or IE11
	Target IE 10 with JavaScript and CSS property detection.
	# 2013 by Tim Pietrusky
	# timpietrusky.com
	**/

	// IE 10 only CSS properties
	var ie10Styles = [
	'msTouchAction',
	'msWrapFlow'];

	var ie11Styles = [
	'msTextCombineHorizontal'];

	/*
	* Test all IE only CSS properties
	*/

	var d = document;
	var b = d.body;
	var s = b.style;
	var brwoser = null;
	var property;

	// Tests IE10 properties
	for (var i = 0; i < ie10Styles.length; i++) {
		property = ie10Styles[i];
		if (s[property] != undefined) {
			brwoser = "ie10";
		}
	}

	// Tests IE11 properties
	for (var i = 0; i < ie11Styles.length; i++) {
		property = ie11Styles[i];
		if (s[property] != undefined) {
			brwoser = "ie11";
		}
	}
	 
	 //Grayscale images only on browsers IE10+ since they removed support for CSS grayscale filter
	 if(brwoser == "ie10" || brwoser == "ie11" ){
		$('body').addClass('ie11'); // Fixes marbin issue on IE10 and IE11 after canvas function on images
		$('.grayscale img').each(function(){
			var el = $(this);
			el.css({"position":"absolute"}).wrap("<div class='img_wrapper' style='display: inline-block'>").clone().addClass('img_grayscale ieImage').css({"position":"absolute","z-index":"5","opacity":"0"}).insertBefore(el).queue(function(){
				var el = $(this);
				el.parent().css({"width":this.width,"height":this.height});
				el.dequeue();
			});
			this.src = grayscaleIe(this.src);
		});
		
		// Quick animation on IE10+ 
		$('.grayscale img').hover(
			function () {
				$(this).parent().find('img:first').stop().animate({opacity:1}, 200);
			}, 
			function () {
				$('.img_grayscale').stop().animate({opacity:0}, 200);
			}
		);
		
		// Custom grayscale function for IE10 and IE11
		function grayscaleIe(src){
			var canvas = document.createElement('canvas');
			var ctx = canvas.getContext('2d');
			var imgObj = new Image();
			imgObj.src = src;
			canvas.width = imgObj.width;
			canvas.height = imgObj.height; 
			ctx.drawImage(imgObj, 0, 0); 
			var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
			for(var y = 0; y < imgPixels.height; y++){
				for(var x = 0; x < imgPixels.width; x++){
					var i = (y * 4) * imgPixels.width + x * 4;
					var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
					imgPixels.data[i] = avg; 
					imgPixels.data[i + 1] = avg; 
					imgPixels.data[i + 2] = avg;
				}
			}
			ctx.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
			return canvas.toDataURL();
		};
	 };

	// If the browser does not support CSS filters filters, we are applying grayscale.js function
	// This part of Grayscale images applies on Opera, Firefox and Safari browsers
	if (!Modernizr.cssfilters) {
		var $images = $(".grayscale img"), imageCount = $images.length, counter = 0;

		// One instead of on, because it need only fire once per image
		$images.one("load",function(){
			// increment counter every time an image finishes loading
			counter++;
			if (counter == imageCount) {
				// do stuff when all have loaded
				grayscale($('.grayscale img'));
				$(".grayscale img").hover(
					function () {
						grayscale.reset($(this));
					}, 
					function () {
						grayscale($(this));
					}
				);
			}
		}).each(function () {
		if (this.complete) {
			// manually trigger load event in
			// event of a cache pull
				$(this).trigger("load");
			}
		});
	}
});