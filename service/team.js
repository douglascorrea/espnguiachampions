angular.module('espnGuiaChampions').factory('Team',function($resource) {

	return $resource('assets/times.json', {}, {
        query: {
            method: 'GET'
        }
    });
});