angular.module('espnGuiaChampions').controller('HomeCtrl', function ($scope, $rootScope, Team, $window) {
    var _ = $window._;
    _.mixin(_.str.exports());

    $scope.slugfy = function(string) {
        return _.slugify(string);
    };

    $rootScope.teams = Team.query(function(data) {
        console.log(data);
    });

    $scope.menu = {};
    $scope.menu.teamNameA = null;
    $scope.menu.teamNameB = null;
    $scope.menu.teamNameC = null;
    $scope.menu.teamNameD = null;
    $scope.menu.teamNameE = null;
    $scope.menu.teamNameF = null;
    $scope.menu.teamNameG = null;
    $scope.menu.teamNameH = null;

});