angular.module('espnGuiaChampions').controller('TeamDesempenhoCtrl',function($scope, $rootScope, Team, $window, $stateParams, $state){
    $rootScope.containerHome = false;
    var _ = $window._;
    _.mixin(_.str.exports());

    $scope.slugfy = function(string) {
        return _.slugify(string);
    };

    $rootScope.teams = Team.query(function(data) {
        $scope.team = _.find(data.times, function(team) {
            if(_.slugify(team.abreviaturaMenu) === $stateParams.id) {
                $('<style>.time .menuTeam ul li.active{background-color: #'+team.codigoCor+';}</style>').appendTo('head');
                $('<style>.time .menuTeam ul li.active:before{border-bottom-color: #'+team.codigoCor+';}</style>').appendTo('head');
                return true;
            } else {
                return false;
            }
        });
    });

    $scope.teamId = $stateParams.id;

    $scope.team = null;

    $scope.menu = {};
    $scope.menu.teamNameA = null;
    $scope.menu.teamNameB = null;
    $scope.menu.teamNameC = null;
    $scope.menu.teamNameD = null;
    $scope.menu.teamNameE = null;
    $scope.menu.teamNameF = null;
    $scope.menu.teamNameG = null;
    $scope.menu.teamNameH = null;
    $scope.navMobile = {};
    $scope.navMobile.team = null;

    $scope.goToTeam = function(){
        $state.go('team',{id: _.slugify($scope.navMobile.team.abreviaturaMenu)});
    };
});