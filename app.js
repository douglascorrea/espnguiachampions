angular.module('espnGuiaChampions', ['ui.bootstrap','ui.utils','ui.router','ngAnimate', 'ngResource', 'ngSanitize', 'cgBusy']);

angular.module('espnGuiaChampions').config(function($stateProvider, $urlRouterProvider) {

    $stateProvider.state('home', {
        url: '/',
        templateUrl: 'partial/home/home.html',
        controller: 'HomeCtrl'
    });
    $stateProvider.state('team', {
        url: '/team/:id',
        templateUrl: 'partial/team/team.html',
        controller: 'TeamCtrl'
    });
    $stateProvider.state('teamEquipe', {
        url: '/team/:id/equipe',
        templateUrl: 'partial/teamEquipe/teamEquipe.html',
        controller: 'TeamEquipeCtrl'
    });
    $stateProvider.state('teamDestaque', {
        url: '/team/:id/destaque',
        templateUrl: 'partial/teamDestaque/teamDestaque.html',
        controller: 'TeamDestaqueCtrl'
    });
    $stateProvider.state('teamDesempenho', {
        url: '/team/:id/desempenho',
        templateUrl: 'partial/teamDesempenho/teamDesempenho.html',
        controller: 'TeamDesempenhoCtrl'
    });
    $stateProvider.state('creditos', {
        url: '/creditos',
        templateUrl: 'partial/creditos/creditos.html'
    });
    /* Add New States Above */
    $urlRouterProvider.otherwise('/');

});

angular.module('espnGuiaChampions').run(function($rootScope) {

    $rootScope.playing = true;

    $rootScope.toggleSom = function() {

        if($rootScope.playing) {
            $rootScope.playing = false;
            document.getElementById('player').pause();
        }else {
            $rootScope.playing = true;
            document.getElementById('player').play();
        }
    };

    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        if (toState.name === 'home') {
            $rootScope.containerHome = true;
        } else {
            $rootScope.containerHome = false;
        }

    });
});
