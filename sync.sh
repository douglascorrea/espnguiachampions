#!/bin/zsh -x
aws s3 sync . s3://espn-guiadachampions --exclude ".git/*" --exclude ".idea/*" --exclude "*.iml" --exclude "temp/*" --exclude "dist/*" --exclude "node_modules/*" --exclude "_SpecRunner.html" --exclude ".DS_Store" --exclude "test-results.xml" --exclude ".*" --exclude "Gruntfile.js" --exclude "gulpfile.js" --exclude "*.json" --exclude "sync.sh" --delete
